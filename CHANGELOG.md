# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html)

## [0.0.2]
### Added
- Add real configuration
- Separate Crawl, send message and commands
- Start/stop receiving notifications
### Changed
- Update time to last_title of RSS feed
- Crawler to Feed
- Put commands on another files
### Fixed
- Not sending post
### Removed
- token.rb

## [0.0.1]
### Added
- Channels and Crawlers
- Send RSS updates
- Unique log
- Cron every hour
- README.md
- Commands
- Usage of Docker

### Changed
- Timestamp by relation

### Fixed
- Can rescue from Crawl

### Removed
- Cron unique file
