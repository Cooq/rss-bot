require_relative 'init'
require 'telegram/bot'
require 'open-uri'
require 'rss'

require_relative 'command/channel'
require_relative 'command/feed'


class Bot

  def initialize
    @commands = []
    @posts = {}
    @bot = nil
  end

  def run
    commands = self.listen_commands
    feeds = self.feed_updates
    send = self.send_posts
    commands.join
    feeds.join
    send.join
  end

  def listen_commands
    return Thread.new do 
      Telegram::Bot::Client.run(CONFIG[:token]) do |bot|
        begin
          @bot = bot
          channel = nil
          bot.listen do |message|
            begin
              if !message.text.nil?
                msg = message.text.downcase
                chat_id = message.chat.id
                Log.info msg
                channel = Channel._add(message)
                
                add_feed(msg, chat_id, bot, channel)
                remove_feed(msg, chat_id, bot, channel)
                list_feed(msg, chat_id, bot, channel)

                start_fetching(msg, chat_id, bot, channel)
                stop_fetching(msg, chat_id, bot, channel)
                help(msg, chat_id, bot, channel)
              end
            end
          end
        rescue Telegram::Bot::Exceptions::ResponseError => error
          puts error
          error_code = JSON.parse(error.response.body)['error_code']
          if error_code == 403
            if channel
              channel.is_fetching = false
              channel.save
            end
            retry
          end
          if error_code == 500
            sleep(10)
            retry
          end
        end
      end
    end
  end

  def feed_updates
    return Thread.new do
      while true
        Log.debug("Start crawl")
        feeds = Feed.all        
        feeds.each do |feed|
          begin
            Log.info("Crawl #{feed.url}")
            rss = URI.open(feed.url, 'User-Agent' => 'Mozilla/5.0 (X11; Linux x86_64; rv:66.0) Gecko/20100101 Firefox/66.0').read
            parsed_rss = RSS::Parser.parse(rss)
            @posts[feed.id] = parsed_rss.items.reverse
          rescue StandardError => e
            Log.error("ERROR on #{feed.url}, #{e}")
            @posts[feed.id] = []
          end
        end
        sleep(CONFIG[:feed_updates_interval])
      end
    end
  end

  def format(item)
    begin
        title = item.title.respond_to?(:content) && item.title.content || item.title
        link = item.link.respond_to?(:href) && item.link.href || item.link
        return "#{title}\n#{link}"
    rescue StandardError => e
        Log.error("ERROR on #{item} #{e}")
        return ""
    end
  end

  def send_posts
    return Thread.new do
      while true
        channels = Channel.where({is_fetching: true})
        Log.debug "Send Posts to #{channels.size} channels"
        
        channels.each do |channel|
          message_count = CONFIG[:feed_post_amount]

          channel.relations.each do |relation|
            next if @posts[relation.feed.id].nil?
              
            is_after_last = false
            if relation.last_title.nil?
              last_fetch_post = @posts[relation.feed.id][-CONFIG[:feed_post_amount]]
              if last_fetch_post then
                relation.last_title = last_fetch_post.title 
              end
            end

            @posts[relation.feed.id].each do |post|
              if is_after_last then
                message_count -= 1
                @bot.api.send_message(chat_id: channel.uid, text: self.format(post))
                relation.last_title = post.title
              end

              is_after_last = true if relation.last_title == post.title
              break if message_count <= 0
            end

            relation.save
            break if message_count <= 0
          end
        end
        sleep(CONFIG[:feed_posts_interval])
      end
    end
  end

end

bot = Bot.new
bot.run