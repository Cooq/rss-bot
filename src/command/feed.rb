def add_feed message, chat_id, bot, channel
  if message.match /\/add ([^ ]+) ([^ ]+)/i then
    match = message.match(/^\/add ([^ ]+) ([^ ]+)/i)
    name = match[1]
    url = match[2]
    if Feed._add(channel.id, name, url) then
      bot.api.send_message(chat_id: chat_id, text: "#{name} added successfuly (url: #{url})")
    else
      bot.api.send_message(chat_id: chat_id, text: "#{url} was already added")                
    end
  end
end

def remove_feed message, chat_id, bot, channel
  if message.match /\/remove ([^ ]+)/i
    name = message.match(/\/remove ([^ ]+)/i)[1]
    Feed._remove(channel.id, name)
    bot.api.send_message(chat_id: chat_id, text: "#{name} removed successfuly")
  end
end

def list_feed message, chat_id, bot, channel
  if message.match /\/list/i
    Log.info("list")
    bot.api.send_message(chat_id: chat_id, text: Channel.format(channel))
  end
end