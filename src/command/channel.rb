HELP_MESSAGE = %q(
  Commands :
  - /add name url (Add new RSS feed)
  - /remove name (Remove feed)
  - /list (List all your feeds)
  - /start (Start getting updates)
  - /stop (Stop messages)
  - /help (Get the help)
)

def start_fetching message, chat_id, bot, channel
  if message.match /\/start/i then
    channel.is_fetching = true
    channel.save
    bot.api.send_message(chat_id: chat_id, text: "I will update you RSS starting now !\n#{HELP_MESSAGE}")
  end
end

def stop_fetching message, chat_id, bot, channel
  if message.match /\/stop/i then
    channel.is_fetching = false
    channel.save
    bot.api.send_message(chat_id: chat_id, text: "I will not bother anymore")
  end
end

def help message, chat_id, bot, channel
  if message.match /\/help/i then
    bot.api.send_message(chat_id: chat_id, text: HELP_MESSAGE)
  end
end
