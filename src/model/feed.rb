class Feed < ActiveRecord::Base
  has_many :relations
  has_many :channels, {through: :relations, source: :channel}
  def to_s
    self.url
  end

  def self._remove(channel_id, name)
    relation = Relation.find_by(name: name)
    relation.destroy if relation 
  end

  def self._add(channel_id, name, url)
    feed = Feed.find_by(url: url)
    if not feed then
      feed = Feed.new
      feed.url = url
      feed.save
    end
    begin
      relation = Relation.new
      relation.channel_id = channel_id
      relation.feed = feed
      relation.name = name
      relation.last_updated = 0
      relation.save
      Log.info relation
      return true
    rescue ActiveRecord::RecordNotUnique => error
      Log.error "Multiple relation"
      return false
    end
  end

end
