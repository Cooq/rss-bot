class Relation < ActiveRecord::Base
  belongs_to :channel, class_name: :Channel
  belongs_to :feed, class_name: :Feed
  has_many :channels
  has_many :feeds
end
