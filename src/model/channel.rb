class Channel < ActiveRecord::Base
  has_many :relations
  has_many :feeds, {through: :relations, source: :feed}

  def self.format(channel)
    s = ""
    channel.relations.each do |rel|
      s += "- #{rel.name} : #{rel.feed.url}\n"
    end
    return s
  end

  def self._add(message)
    id = message.chat.id
    channel = Channel.find_by(uid: id)
    if not channel then
      channel = Channel.new
      channel.uid = id
      channel.save
    end
    Log.info channel
    return channel
  end
end
