require 'sqlite3'
require 'active_record'

ActiveRecord::Base.establish_connection( adapter: 'sqlite3', database: CONFIG[:db_path] )

if !ActiveRecord::Base.connection.data_source_exists? 'channels' then
  Log.debug 'create channels and crawlers'
  class InitialSchema < ActiveRecord::Migration[5.0]
    def self.up
      create_table :channels do |t|
        t.string :uid
        t.timestamps
      end
      create_table :relations do |t|
        t.references :channel
        t.references :crawler
        t.timestamp :last_updated
        t.string :name
      end
      create_table :crawlers do |t|
        t.string :url
        t.timestamps
      end
      add_index :channels, :uid, unique: true
      add_index :relations, [:channel_id, :crawler_id], unique: true
    end

    def self.down
      drop_table :channels
      drop_table :relations
      drop_table :crawlers
    end
  end

  InitialSchema.migrate(:up)
end
