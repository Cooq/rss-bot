require 'sqlite3'
require 'active_record'

ActiveRecord::Base.establish_connection( adapter: 'sqlite3', database: CONFIG[:db_path] )

if !ActiveRecord::Base.connection.data_source_exists? 'feeds' then
  Log.debug 'rename crawlers'
  class InitialSchema < ActiveRecord::Migration[5.0]
    def self.up
      rename_table :crawlers, :feeds
      rename_column :relations, :crawler_id, :feed_id
      add_column :relations, :last_title, :string
      add_column :channels, :is_fetching, :boolean
    end

    def self.down
      rename_table :feeds, :crawlers
      rename_column :relations, :feed_id, :crawler_id
      remove_column :relations, :last_title
      remove_column :channels, :is_fetching, :boolean
    end
  end

  InitialSchema.migrate(:up)
end
