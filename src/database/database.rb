require 'sqlite3'
require 'active_record'

Log.debug "Log file created"

# Require all patches
Dir[File.join(__dir__, 'patchs', '*.rb')].each { |file| require file }