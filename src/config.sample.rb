
CONFIG = {
  "token": "",
  "feed_updates_interval": 600, # In seconds
  "feed_posts_interval": 300, # In seconds
  "feed_post_amount": 5,
  "log_level": "debug",
  "db_path": "./db/dev.db"
}
