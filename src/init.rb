require 'logger'
require_relative 'config'

Log = Logger.new("log/current.log")
Log.level = CONFIG[:log_level]
Log.debug CONFIG

require_relative 'database/database'
require_relative 'model/channel'
require_relative 'model/feed'
require_relative 'model/relation'