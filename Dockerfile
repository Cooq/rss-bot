  # Only 26Mo
FROM ruby:alpine

COPY . /app
WORKDIR /app

ARG TOKEN
RUN echo "Token=\""$TOKEN"\"" > src/token.rb

RUN apk add build-base sqlite-dev
RUN bundle install
RUN apk del build-base 

CMD ["bundle", "exec", "ruby", "src/bot.rb"]